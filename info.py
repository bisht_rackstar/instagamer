import os
import errno

directory = "Edit_pic\\"
  
# Parent Directory path
path = 'C:\\Users\\Igris\\Pictures\\joker\\'

# copy_path = 'C:\\Users\\Igris\\Pictures\\edit_picture\\cop\\' 
# Out Path
out_path = os.path.join(path, directory)
  
# Create the directory
try:
    os.mkdir(out_path)
    print("Directory '%s' created" %directory)
except OSError as exc:
    if exc.errno != errno.EEXIST:
        raise
    pass

# remove me file extension 
rm_me = ".REMOVE_ME"

# image file extension
ext = (".jpg",".jpeg",".png")

# post caption
caption = '''What do you think about it? ✨🍁
.
.
.
.
Follow : @camouflage.life if you appreciate our contents🌻
.
.
.
.
#quoteoftheday
#quotationoftheday
#lifelesson
#wisewords
#lifequote

#animeaesthetic
#animepic
#animevibe
#animeedit
#animeaestheticedit

#relation
#friendshipvibes
#relationshipsbelike
#relationshipthoughts


#wordsofwisdom 
#wiseword
#wordofwisdom

#jokerquote 
#jokerword
#jokerstory 
#jokerquotespage

#lifewisdom
#lifemotivation 
#lifeinspiration
#lifelesson
#lifequote'''

error = '''


        \          SORRY            /
         \                         /
          \  This Directory does  /
           ]   not exist yet.    [    ,'|
           ]                     [   /  |
           ]___               ___[ ,'   |
           ]  ]\             /[  [ |:   |
           ]  ] \           / [  [ |:   |
           ]  ]  ]         [  [  [ |:   |
           ]  ]  ]__     __[  [  [ |:   |
           ]  ]  ] ]\ _ /[ [  [  [ |:   |
           ]  ]  ] ] (#) [ [  [  [ :===='
           ]  ]  ]_].nHn.[_[  [  [
           ]  ]  ]  HHHHH. [  [  [
           ]  ] /   `HH("N  \ [  [
           ]__]/     HHH  "  \[__[
           ]         NNN         [
           ]         N/"         [
           ]         N H         [
          /          N            \
         /           q,            \
        /                           \
            '''
