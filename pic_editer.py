from PIL import ImageOps
from PIL import Image

def add_border(input_image, output_image, border):
    img = input_image

    if isinstance(border, int) or isinstance(border, tuple):
        bimg = ImageOps.expand(img, border=border)
    else:
        raise RuntimeError('border is not an integer or tuple!')

    bimg.save(output_image)

def editor(post, pic):
    img = Image.open(post)
    img_wid = img.width
    img_hig = img.height
    if img_wid < img_hig:
        img_size = img_hig-img_wid
        add_border(img,
                output_image=(pic),
                border=(img_size, 0))
    elif img_wid > img_hig:
        img_size = img_hig-img_wid
        add_border(img,
                output_image=(pic),
                border=(0, img_size))
    else:
        print("somthing is wrong")
